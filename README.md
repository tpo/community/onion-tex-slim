# Onion TeX Slim

## About

Onion TeX Slim is a modern and simple slide compiler for [LaTeX][]'s [Beamer][]
document class inspired by [The Tor Project](https://www.torproject.org/)'s new
and fancy [styleguide](https://styleguide.torproject.org/).

* This is fork from [onion-tex][], slimmed down to include the minimum.
* To keep the repository size low, the fork has no common Git history, nor custom slides.
* LICENSE and authorship is retained, though.
* Useful to use it as a submodule like in the [anarcat's presentations][] repository,
  but without the need to checkout lots of files.

The Onion TeX Slim repository is located at [https://gitlab.torproject.org/rhatto/onion-tex-slim][].

[LaTeX]: https://www.latex-project.org
[Beamer]: https://en.wikipedia.org/wiki/Beamer_(LaTeX)https://en.wikipedia.org/wiki/Beamer_(LaTeX)
[onion-tex]: https://gitlab.torproject.org/ahf/onion-tex
[anarcat's presentations]: https://gitlab.torproject.org/anarcat/presentations
[https://gitlab.torproject.org/rhatto/onion-tex-slim]: https://gitlab.torproject.org/rhatto/onion-tex-slim

## Requirements

Onion TeX Slim requires the following software:

* [Pandoc](https://pandoc.org).
* [Latexmk](https://personal.psu.edu/jcc8/software/latexmk-jcc) (maybe this is not a strict dependency)
* [TeX Live](https://tug.org/texlive) with additional packages ([texlive-latex-extra][] on [Debian][])

Optional dependencies:

* [GNU Make](https://www.gnu.org/software/make) for building through Makefiles.

An example installation scrip tested on [Debian][] Bookworm is available at
[scripts/onion-tex-slim-provision-build][].

[Debian]: https://www.debian.org
[texlive-latex-extra]: https://tracker.debian.org/pkg/texlive-extra
[scripts/onion-tex-slim-provision-build]: scripts/onion-tex-slim-provision-build

## Installation

This repository is ready to be used in any existing repository.
It can be installed in a number of ways as follows.

## Forking the project

Simply fork this project and change whatever you need.

## As a Git submodule

You can simply put it as a Git submodule somewhere, like in a `vendors` folder of your project:

    mkdir -p vendor && \
    git submodule add --depth 1 \
      https://gitlab.torproject.org/tpo/community/onion-tex-slim vendors/onion-tex-slim

Then use symbolic links to reference all the needed files from the root of your
repository.

## Manually copying

Even simpler, copy all the relevant files from this repository into your project.

## Outside the project

* The Onion TeX Slim repository can be installed anywhere in your system.
* In fact, it does not need to be stored inside your project if you don't want
  to.
* You just need to make sure to invoke the scripts from your project folder or
  pass the appropriate folder or file path as arguments to it's scripts.

## Other ways

* `git-subtree(1)`.
* ?

## Installation notes

* If you plan to use either GitLab CI/CD or GitHub Actions to build your
  documentation, make sure to at least copy/adapt the corresponding files from
  the submodule.
* You can't just symlink those since the submodule won't be accessible when
  GitLab and GitHub looks for CI/CD or Actions files.
* The provided CI configuration takes care of initializing the submodule during
  build time, but they should be available in the main repo in order to do so.

## Repository layout

Example repository layout:

    * /path/to/my/project
        * slides
          * slide-for-conference-1
              * slide-for-conference-1.md
          * slide-for-conference-2
              * slide-for-conference-2.md
        * vendors
            * onion-tex-slim

## Building all slides

Onion TeX Slim can be used to build [Beamer][] PDFs from any Markdown file it
finds, so it's recommened to place it in the top-level folder of your project
slides folder.

Once all dependencies are installed and the required files are in place in your
repository, just run the build script:

    vendors/onion-tex-slim/scripts/onion-tex-slim-build-all slides/

## Building a single slide

If invoked without arguments, the script above will build slides from all
Markdown file it finds recursively from the current folder.

You can also build a single slide deck:

    vendors/onion-tex-slim/scripts/onion-tex-slim-build \
      slides/slide-for-conference-1/slide-for-conference-1.md

## Makefile

Onion TeX slim also comes with a [sample Makefile][] that can be customized to
your needs, placed in the desired folder and build all your slides through

    make onion-tex-slim-build-all

You can also build specific slides using `make`:

    make -C slides slides/slide-for-conference-1/slide-for-conference-1.pdf

[sample Makefile]: https://gitlab.torproject.org/tpo/community/onion-tex-slim/-/blob/main/Makefile.onion-tex-slim

## Continuous Integration

Onion TeX Slim support Continuous Integration (CI) through GitLab.
Check the [.gitlab-ci-onion-tex-slim.yml][] file for details.

[.gitlab-ci-onion-tex-slim.yml]: .gitlab-ci-onion-tex-slim.yml

## TODO and issues

Check existing and report new issues in the [ticket queue][].

[ticket queue]: https://gitlab.torproject.org/tpo/community/onion-tex-slim/-/issues
